Pod::Spec.new do |s|

  s.name         = "UIColor+Hex"
  s.version      = "0.0.2"
  s.summary      = "Extension of UIColor to init with Hex string"
  s.homepage     = "https://bitbucket.org/bazaku/uicolor-hex/overview"
  s.license      = "MIT"
  s.author       = { "Siwasit Anmahapong" => "siwasit.anmahapong@allianz.com" }
  s.source       = { :git => "https://bazaku@bitbucket.org/bazaku/uicolor-hex.git", :tag => s.version }
  s.source_files = "Classes"
  s.requires_arc = true
  s.framework 		= 'UIKit'
  
end
