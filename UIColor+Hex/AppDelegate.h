//
//  AppDelegate.h
//  UIColor+Hex
//
//  Created by Siwasit Anmahapong on 6/17/16.
//  Copyright © 2016 Siwasit Anmahapong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

